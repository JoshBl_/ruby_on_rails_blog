class Article < ApplicationRecord
    #it's important to note that this class inherits from ApplicationRecord, which in turn inherits from ActiveRecord::Base
    #which provides basic database CRUD (Create, Read, Update and Destroy), data validation, search and ability to relate multiple models to one another
    validates :title, presence: true, length: {minimum: 5}
    #this ensures that the title must be present and a minimum of 5 characters long
end
