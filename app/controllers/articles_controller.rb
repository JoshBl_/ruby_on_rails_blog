class ArticlesController < ApplicationController
  #defining a new action inside the container
  
  #it's common practice to define the index action as the very first action in a controller
  #every actions needs a view!
  def index
    @articles = Article.all
  end
  
  def show
    #find is used with the article ID - used an instance variable ('@article') to hold a reference to the article object (Rails will pass all instance variables to the view)
    @article = Article.find(params[:id])
  end
  
  def new
    @article = Article.new
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  #when a form is submitted - the fields of the form are sent to Rails as parameters
  #the parameters are then referenced inside the controller actions (like the action below!) to perform a task
  #the params method is the object which represents coming in from the form.
  #the params method returns an ActionController::Parameters object, which allows you to access the keys of the hash using keys or symbols.
  def create
    #the render method is taking a hash with a key of plain and a value of 'params[:article].inspect'
    #the params method is the object which represents the parameters coming in from the form
    #this return ActionController::Parameters object - this gives access to the hash using keys or symbols (in this situation, the params from the form that matter)
    #render plain: params[:article].inspect
    
    #here we are using the new Article model to save items to the database
    #Rails models can be initialised with its respective attributes - which are automatically mapped to the respective database columns
    #Article is a reference to the class named Article (which is in models > article.rb) Class names in Ruby must start with a capital letter!
    
    #we need to prevent wrongful mass assignment, we need to allow title and text for valid use of create!
    @article = Article.new(article_params)
    
    #responsible for saving the model in the database - then we direct the user to show the action
     if @article.save
       #this tells the browser to issue another request
       redirect_to @article
     else
       #use render here instead of redirect_to so the object is passed back to the new template when it is rendered
       render 'new'
     end
  end
  
  def update
    @article = Article.find(params[:id])
    
    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end
  
  #you can call destroy on Active Record objects when you want to delete them from the database.
  #no view necessary, we're redirecting to the index action
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
 
    redirect_to articles_path
  end
  
  #kept it in its own method so it can be used across multiple actions
  #made private so it can't be called outside its intended context
  private
  def article_params
    params.require(:article).permit(:title, :text)
  end
end
