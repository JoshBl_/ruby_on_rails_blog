Rails.application.routes.draw do
  get 'welcome/index'
  get 'articles/new'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  #resources are a collection of similar objects (like people and animals)
  #CRUD = Create Read Update Destroy
  #You can apply CRUD operations to items of a resource
  resources :articles
  
  root 'welcome#index'
end

#run 'rails routes' to see defined routes 