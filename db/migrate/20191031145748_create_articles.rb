#created by using the generate command to create a model with two attributes (with types)
#these attibutes are automatically added to the Articles database and mapped to the Article model
#this file is responsible for creating the database structure
#Migrations are a Ruby class which make it simple to create and modify database tables
#uses Rake commands to run migrations (you can undo a migration!)

class CreateArticles < ActiveRecord::Migration[6.0]
  #new method called change - this action is also reversable 
  #this will create an articles table with a string and text column (and a timestamp field for creation and update times)
  #run by using rails db:migrate
  def change
    create_table :articles do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
